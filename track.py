#!/usr/bin/env python

import cv2
import serial
from time import sleep

CAP_WIDTH = 1280
CAP_HEIGHT = 720

casc = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
ser = serial.Serial('/dev/ttyACM0', 9600, timeout=5)
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, CAP_WIDTH)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, CAP_HEIGHT)

def rotate(x, y):
    ser.write(bytes([0, x, y]))

while True:
    ret, frame = cap.read()

    faces = casc.detectMultiScale(
        cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY),
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
    )

    if len(faces) == 1:
        (x, y, w, h) = faces[0]
        cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 4)

        xx = (x + w/2 - CAP_WIDTH/2) / CAP_WIDTH
        yy = (y + h/2 - CAP_HEIGHT/2) / CAP_HEIGHT
        xa = int(90 - 75*xx)
        ya = int(115 + 60*yy)

        print(xa, ya)
        rotate(xa, ya)

    cv2.imshow('Video', frame)
    if cv2.waitKey(16) == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
